extern crate byteorder;

mod message;
mod command;
mod frame;

use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::collections::HashMap;

use frame::Frame;


fn main() {
    let mut file = BufReader::new(File::open("test.mvd").unwrap());
    let mut demo_time = 0.0f64;
    // let mut commands_count = HashMap::new();
    loop {
        match Frame::from_reader(&mut file) {
            Err(_) => break,
            Ok(frame) => {
                demo_time += frame.delta_time;
                println!("{:.5}: {:?}", demo_time, frame);
                // let counter = commands_count.entry(frame.commands).or_insert(0);
                // *counter += 1;
            }
        }
    }

    // let mut commands_count = commands_count.into_iter().collect::<Vec<_>>();
    // commands_count.sort_by(|a, b| b.1.cmp(&a.1));

    // for (k, v) in commands_count {
    //     println!("{:?} {}", k, v);
    // }
}
