#[derive(Debug)]
pub enum Message {
    Cmd,
    Read,
    Set,
    Multiple { players: u32 },
    Single { player: u8 },
    Stats { player: u8 },
    All
}

impl Message {
    pub fn from_u8(x: u8) -> Message {
        match x & 7 {
            0 => Message::Cmd,
            1 => Message::Read,
            2 => Message::Set,
            3 => Message::Multiple { players: 0},
            4 => Message::Single { player: x >> 3 },
            5 => Message::Stats { player: x >> 3 },
            6 => Message::All,
            _ => Message::Read
        }
    }
}
