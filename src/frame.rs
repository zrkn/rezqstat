use std::io::{Result, Read};

use byteorder::{LittleEndian, ReadBytesExt};

use message::Message;
use command::Command;


#[derive(Debug)]
pub struct Frame {
    pub delta_time: f64,
    pub message: Message,
    pub commands: Vec<Command>,
    pub size: u32,
}

impl Frame {
    pub fn from_reader(reader: &mut Read) -> Result<Frame> {
        let delta_time = reader.read_u8()? as f64 * 0.001;

        let message = Message::from_u8(reader.read_u8()?);
        let message_size = reader.read_u32::<LittleEndian>()?;
        let mut message_body = reader.take(message_size as u64);

        let mut commands = Vec::new();
        loop {
            let command_res = Command::from_reader(&mut message_body);
            match command_res {
                Ok(command) => {
                    match command {
                        Command::ChokeCount(..) => {},
                        _ => {
                            let mut buffer = Vec::new();
                            message_body.read_to_end(&mut buffer);
                        }
                    }
                    commands.push(command);
                },
                Err(_) => {
                    return Ok(Frame {
                        delta_time: delta_time,
                        message: message,
                        commands: commands,
                        size: message_size,
                    });
                },
            }
        }
    }
}

